const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const postsRoutes = require('./routes/posts');
const userRoutes = require('./routes/user');

// create express app
const app = express();
const dbUri = 'mongodb://cluster0-shard-00-00-5ftdp.mongodb.net:27017,cluster0-shard-00-01-5ftdp.mongodb.net:27017,cluster0-shard-00-02-5ftdp.mongodb.net:27017/node-angular?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin';

const options = {
  user: process.env.MONGO_ATLAS_USER,
  pass: process.env.MONGO_ATLAS_PW,
  useNewUrlParser : true
};

mongoose.connect(
  dbUri,
  options
)
  .then((mongoose) => {
    // const admin = mongoose.mongo.Admin(mongoose.connection.db);
    // admin.buildInfo((err, info) => {
    //   console.log(info.version);
    // });
    console.log('Connected to DB');
  })
  .catch((err) => {
    console.log('Connection failed');
    console.log(err);
  });

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/images', express.static(path.join('backend/images')));

// set the right headers for cross domain
app.use((req, res, next) => {
  // * --> No matter which domain the app is sending the request
  // is running on is allowed to access
  res.setHeader('Access-Control-Allow-Origin', '*');
  // Restrict to domain with a certain set of headers, beside the defaults
  res.setHeader(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept, Authorization'
  );
  // which http verbs are allowed
  // OPTIONS will be sent implicity by the browser
  // to check wether the request is valid, therefor it should
  // be allowed
  res.setHeader(
    'Access-Control-Allow-Methods',
    'GET, POST, PATCH, PUT, DELETE, OPTIONS'
  );
  next();
});

app.use('/api/posts', postsRoutes);
app.use('/api/user', userRoutes);

module.exports = app;
