import { AbstractControl } from '@angular/forms';

import { Observable, Observer, of } from 'rxjs';

/**
 *https://stackoverflow.com/questions/18299806/how-to-check-file-mime-type-with-javascript-before-upload/29672957#29672957
 *
 *To get the bonafide MIME type of a client-side file we can go a step further
 *and inspect the first few bytes of the given file to compare against so-called magic numbers.
 *Be warned that it's not entirely straightforward because, for instance,
 *JPEG has a few "magic numbers". This is because the format has evolved since 1991.
 *
 */

// {[key: string]}: any ==> means this will a dynamic property key
// as string with "any" value
export const mimeType = (control: AbstractControl): Promise<{ [key: string]: any }> | Observable<{ [key: string]: any }> => {
  if (typeof(control.value) === 'string') {
    return of(null);
  }
  const file = control.value as File;
  const fileReader = new FileReader();
  const frObs = Observable.create((observer: Observer<{ [key: string]: any }>) => {
    // 'loadend' has more informations about the file
    fileReader.addEventListener('loadend', () => {
      // allows to read certain patterns in the file metadata
      const arr = new Uint8Array(fileReader.result).subarray(0, 4);
      let header = '';
      let isValid = false;
      for (let i = 0; i < arr.length; i++) {
        // convert the value to HEX
        header += arr[i].toString(16);
      }
      switch (header) {
        case '89504e47':     // png
        case '47494638':     // gif
        case 'ffd8ffe0':     // jpg ...
        case 'ffd8ffe1':
        case 'ffd8ffe2':
        case 'ffd8ffe3':
        case 'ffd8ffe8':
          isValid = true;
          break;
        default:
          isValid = false;
          break;
      }
      if (isValid) {
        observer.next(null);
      } else {
        observer.next({ invalidMimeType: true });
      }
      observer.complete();
    });
    // use ArrayBuffer to easily convert to Uint8Array
    fileReader.readAsArrayBuffer(file);
  });
  return frObs;
};
